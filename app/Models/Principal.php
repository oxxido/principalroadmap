<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Principal extends Model
{
    use userTrait;

    public $incrementing = false;

    public $timestamps = false;

    public function lessons()
    {
        return $this->belongsToMany(Lesson::class)->withTimestamps()->withPivot('started_at', 'completed_at');
    }

    public static function make($attributes = [], User $user = null)
    {
        return self::makeRelation('principal', $attributes, $user);
    }
}
