<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    use UserTrait;

    public $incrementing = false;

    public $timestamps = false;

    public function roadmaps()
    {
        return $this->hasMany(Roadmap::class);
    }

    public static function make($attributes = [], User $user = null)
    {
        return self::makeRelation('admin', $attributes, $user);
    }
}
