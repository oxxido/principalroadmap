<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;

class Lesson extends Model implements SluggableInterface
{
    use ManyToManyRelationTrait, SluggableTrait, TaggableTrait;

    protected $sluggable = ['build_from' => 'title'];

    public function principals()
    {
        return $this->models(Principal::class)->withPivot('id');
    }

    public function addPrincipal($id)
    {
        return $this->addModel(Principal::class, 'principal_id', 'id', $id);
    }

    public function addPrincipals($ids)
    {
        return $this->addModels(Principal::class, 'principal_id', 'id', $ids);
    }

    public function removePrincipal($id)
    {
        return $this->removeModel(Principal::class, 'id', $id);
    }

    public function removePrincipals($ids)
    {
        return $this->removeModels(Principal::class, 'id', $ids);
    }

    public function setPrincipals($ids)
    {
        return $this->setModels(Principal::class, 'id', $ids);
    }

    public function hasPrincipal($id)
    {
        return $this->hasModel(Principal::class, 'principal_id', 'id', $id);
    }

    public function hasPrincipals($ids)
    {
        return $this->hasModels(Principal::class, 'principal_id', 'id', $ids);
    }

    public function resources()
    {
        return $this->models(Resource::class)->withPivot('id');
    }

    public function addResource($id)
    {
        return $this->addModel(Resource::class, 'resource_id', 'id', $id);
    }

    public function addResources($ids)
    {
        return $this->addModels(Resource::class, 'resource_id', 'id', $ids);
    }

    public function removeResource($id)
    {
        return $this->removeModel(Resource::class, 'id', $id);
    }

    public function removeResources($ids)
    {
        return $this->removeModels(Resource::class, 'id', $ids);
    }

    public function setResources($ids)
    {
        return $this->setModels(Resource::class, 'id', $ids);
    }

    public function hasResource($id)
    {
        return $this->hasModel(Resource::class, 'resource_id', 'id', $id);
    }

    public function hasResources($ids)
    {
        return $this->hasModels(Resource::class, 'resource_id', 'id', $ids);
    }

    /**
     * Get the value of the model's route key.
     *
     * @return mixed
     */
    public function getRouteKey()
    {
        return $this->slug;
    }
}
