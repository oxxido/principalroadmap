<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    use userTrait;

    public $incrementing = false;

    public $timestamps = false;

    public function modules()
    {
        return $this->hasMany(Module::class);
    }

    public static function make($attributes = [], User $user = null)
    {
        return self::makeRelation('author', $attributes, $user);
    }
}
