<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;

class Module extends Model implements SluggableInterface
{
    use SluggableTrait, TaggableTrait;

    protected $sluggable = ['build_from' => 'title'];

    public function author()
    {
        return $this->belongsTo(Author::class);
    }

    /**
     * Get the value of the model's route key.
     *
     * @return mixed
     */
    public function getRouteKey()
    {
        return $this->slug;
    }
}
