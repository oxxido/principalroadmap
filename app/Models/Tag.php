<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public function roadmaps()
    {
        return $this->morphedByMany(Roadmap::class, 'taggable')->withPivot('name');
    }

    public function modules()
    {
        return $this->morphedByMany(Module::class, 'taggable')->withPivot('name');
    }

    public function lessons()
    {
        return $this->morphedByMany(Lesson::class, 'taggable')->withPivot('name');
    }
}
