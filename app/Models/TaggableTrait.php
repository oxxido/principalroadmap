<?php

namespace App\Models;

Trait TaggableTrait
{
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable')->withPivot('name');
    }
}
