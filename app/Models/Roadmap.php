<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;

class Roadmap extends Model implements SluggableInterface
{
    use SluggableTrait, TaggableTrait;

    protected $sluggable = ['build_from' => 'title'];

    public function admin()
    {
        return $this->belongsTo(Admin::class);
    }

    /**
     * Get the value of the model's route key.
     *
     * @return mixed
     */
    public function getRouteKey()
    {
        return $this->slug;
    }
}
