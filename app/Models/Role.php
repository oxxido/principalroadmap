<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use ManyToManyRelationTrait;

    const ADMIN = 'admin';
    const AUTHOR = 'author';
    const PRINCIPAL = 'principal';

    public function permissions()
    {
        return $this->models(Permission::class)->withPivot('id');
    }

    public function addPermission($name)
    {
        return $this->addModel(Permission::class, 'permission_id', 'name', $name);
    }

    public function addPermissions($names)
    {
        return $this->addModels(Permission::class, 'permission_id', 'name', $names);
    }

    public function removePermission($name)
    {
        return $this->removeModel(Permission::class, 'name', $name);
    }

    public function removePermissions($names)
    {
        return $this->removeModels(Permission::class, 'name', $names);
    }

    public function setPermissions($names)
    {
        return $this->setModels(Permission::class, 'name', $names);
    }

    public function users()
    {
        return $this->models(User::class)->withPivot('id');
    }

    public function addUser($name)
    {
        return $this->addModel(User::class, 'user_id', 'id', $name);
    }

    public function addUsers($names)
    {
        return $this->addModels(User::class, 'user_id', 'id', $names);
    }

    public function removeUser($name)
    {
        return $this->removeModel(User::class, 'id', $name);
    }

    public function removeUsers($names)
    {
        return $this->removeModels(User::class, 'id', $names);
    }

    public function setUsers($names)
    {
        return $this->setModels(User::class, 'id', $names);
    }
}
