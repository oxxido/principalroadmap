<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['domain' => Config::get('app.admin_domain'), 'namespace' => 'Admin', 'as' => 'admin.'], function () {
    Route::group(['middleware' => ['ACL']], function () {
        Route::resource('roadmaps', 'RoadmapsController');
        Route::resource('modules', 'ModulesController');
        Route::resource('lessons', 'LessonsController');
        Route::resource('resources', 'ResourcesController');
        Route::resource('tags', 'TagsController');
    });
    Route::get('/', ['name' => 'admin.home', 'uses' => 'HomeController@index']);
});

Route::get('admin', function () {
    return view('admin.index');
});
