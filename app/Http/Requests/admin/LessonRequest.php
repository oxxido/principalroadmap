<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class LessonRequest extends Request
{
    public function attributes()
    {
        return self::baseAttributes();
    }

    public static function baseAttributes()
    {
        return [
            'title'       => trans('lesson.fields.title'),
            'image_url'   => trans('lesson.fields.image_url'),
            'description' => trans('lesson.fields.description'),
            'bonus'       => trans('lesson.fields.bonus'),
            'order'       => trans('lesson.fields.order'),
        ];
    }
}
