<?php

namespace App\Http\Requests\Admin;

class ResourceUpdateRequest extends RoadmapRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return self::baseRules();
    }

    public static function baseRules()
    {
        $create_rules = ResourceStoreRequest::baseRules();

        $rules = [];

        return array_merge($create_rules, $rules);
    }
}
