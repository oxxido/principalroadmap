<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class ModuleRequest extends Request
{
    public function attributes()
    {
        return self::baseAttributes();
    }

    public static function baseAttributes()
    {
        return [
            'title'       => trans('module.fields.title'),
            'image_url'   => trans('module.fields.image_url'),
            'description' => trans('module.fields.description'),
            'bonus'       => trans('module.fields.bonus'),
            'order'       => trans('module.fields.order'),
        ];
    }
}
