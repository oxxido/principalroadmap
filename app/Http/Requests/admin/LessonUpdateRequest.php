<?php

namespace App\Http\Requests\Admin;

class LessonUpdateRequest extends RoadmapRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return self::baseRules();
    }

    public static function baseRules()
    {
        $create_rules = LessonStoreRequest::baseRules();

        $rules = [];

        return array_merge($create_rules, $rules);
    }
}
