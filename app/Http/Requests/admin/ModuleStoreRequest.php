<?php

namespace App\Http\Requests\Admin;

class ModuleStoreRequest extends RoadmapRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return self::baseRules();
    }

    public static function baseRules()
    {
        return [
            'title' => 'required|max:255',
            'order' => 'required|integer|min:1'
        ];
    }
}
