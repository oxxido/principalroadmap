<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class RoadmapRequest extends Request
{
    public function attributes()
    {
        return self::baseAttributes();
    }

    public static function baseAttributes()
    {
        return [
            'title'       => trans('roadmap.fields.title'),
            'image_url'   => trans('roadmap.fields.image_url'),
            'description' => trans('roadmap.fields.description'),
        ];
    }
}
