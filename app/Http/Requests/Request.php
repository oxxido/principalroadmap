<?php

namespace App\Http\Requests;

use Illuminate\Routing\Route;
use Illuminate\Foundation\Http\FormRequest;

abstract class Request extends FormRequest
{
    protected $route;

    public function __construct(Route $route)
    {
        parent::__construct();
        $this->route = $route;
    }
}
