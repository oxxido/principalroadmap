<?php

namespace App\Http\Controllers\Admin;

use App\Models\Lesson;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\LessonStoreRequest;
use App\Http\Requests\Admin\LessonUpdateRequest;

class LessonsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return \View::make('admin.lessons.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return \View::make('admin.lessons.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param LessonStoreRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(LessonStoreRequest $request)
    {
        return \Redirect::to('admin.lessons.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Lesson $lesson
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show(Lesson $lesson)
    {
        return \Redirect::to('admin.lessons.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Lesson $lesson
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Lesson $lesson)
    {
        return \View::make('admin.lessons.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param LessonUpdateRequest $request
     * @param Lesson              $lesson
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(LessonUpdateRequest $request, Lesson $lesson)
    {
        return \Redirect::back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Lesson $lesson
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Lesson $lesson)
    {
        return \Redirect::to('admin.lessons.index');
    }
}
