<?php

namespace App\Http\Controllers\Admin;

use App\Models\Module;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ModuleStoreRequest;
use App\Http\Requests\Admin\ModuleUpdateRequest;

class ModulesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return \View::make('admin.modules.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return \View::make('admin.modules.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ModuleStoreRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ModuleStoreRequest $request)
    {
        return \Redirect::to('admin.modules.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Module $module
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show(Module $module)
    {
        return \Redirect::to('admin.modules.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Module $module
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Module $module)
    {
        return \View::make('admin.modules.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ModuleUpdateRequest $request
     * @param Module              $module
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ModuleUpdateRequest $request, Module $module)
    {
        return \Redirect::back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Module $module
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Module $module)
    {
        return \Redirect::to('admin.modules.index');
    }
}
