<?php

namespace App\Http\Controllers\Admin;

use App\Models\Roadmap;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\RoadmapStoreRequest;
use App\Http\Requests\Admin\RoadmapUpdateRequest;

class RoadmapsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return \View::make('admin.roadmaps.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return \View::make('admin.roadmaps.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RoadmapStoreRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(RoadmapStoreRequest $request)
    {
        return \Redirect::to('admin.roadmaps.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Roadmap $roadmap
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show(Roadmap $roadmap)
    {
        return \Redirect::to('admin.roadmaps.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Roadmap $roadmap
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Roadmap $roadmap)
    {
        return \View::make('admin.roadmaps.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param RoadmapUpdateRequest $request
     * @param Roadmap              $roadmap
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(RoadmapUpdateRequest $request, Roadmap $roadmap)
    {
        return \Redirect::back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Roadmap $roadmap
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Roadmap $roadmap)
    {
        return \Redirect::to('admin.roadmaps.index');
    }
}
