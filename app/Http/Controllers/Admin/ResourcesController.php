<?php

namespace App\Http\Controllers\Admin;

use App\Models\Resource;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ResourceStoreRequest;
use App\Http\Requests\Admin\ResourceUpdateRequest;

class ResourcesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return \View::make('admin.resources.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return \View::make('admin.resources.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ResourceStoreRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ResourceStoreRequest $request)
    {
        return \Redirect::to('admin.resources.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Resource $resource
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show(Resource $resource)
    {
        return \Redirect::to('admin.resources.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Resource $resource
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(Resource $resource)
    {
        return \View::make('admin.resources.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param ResourceUpdateRequest $request
     * @param Resource              $resource
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(ResourceUpdateRequest $request, Resource $resource)
    {
        return \Redirect::back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Resource $resource
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Resource $resource)
    {
        return \Redirect::to('admin.resources.index');
    }
}
