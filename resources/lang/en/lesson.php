<?php

return [
    'fields' => [
        'title'       => 'Title.',
        'image_url'   => 'Link to the an image.',
        'description' => 'Description.',
        'bonus'       => 'Is this bonus material (not required to complete the module)?',
        'order'       => 'Order in which this should be taken.',
    ]
];
