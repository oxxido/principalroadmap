<?php

return [
    'fields' => [
        'image_url'   => 'Link to the an image.',
        'title'       => 'Title.',
        'description' => 'Description.',
    ]
];
