<?php

return [
    'fields' => [
        'title'       => 'Title.',
        'url'         => 'Link to the a resource.',
        'description' => 'Description.',
    ]
];
