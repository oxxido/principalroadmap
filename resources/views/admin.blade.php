<!doctype html>
<html>
<head>
    @include('admin.includes.head')
    @yield('head')
</head>
<body class="skin-blue fixed sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">
        @include('admin.includes.header')

        @yield('body')

        @include('admin.includes.footer')

        @yield('footer')
        
    </div><!-- ./wrapper -->
</body>
</html>