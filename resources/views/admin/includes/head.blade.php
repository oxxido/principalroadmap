<meta charset="UTF-8">
    <title>VIF PR</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- CSS -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css" >
    <link href="http://cc.xeedlabs.com/vendor/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" >
    <link href="{{ asset('css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css" >

    <link href="{{ asset('css/AdminLTE.blue.min.css') }}" rel="stylesheet" type="text/css" >
    <link href="{{ asset('css/skin-green.css') }}" rel="stylesheet" type="text/css" >

    <!-- Dashboard css-->
    

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script type="text/javascript">
            /* does main namespace exists? */
        if (!cc) {
            var cc = {};
        }

        /*set variables and "constants"*/
        cc.ver = "0.0.1";
        cc.baseUrl = "{{url()}}/";
        cc._token = '{{ csrf_token() }}';
        
        cc.pusher_key = "{{ env('PUSHER_KEY') }}";
    </script>