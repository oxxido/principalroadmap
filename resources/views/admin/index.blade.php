@extends('admin')


@section('body')
   
      @include('admin.includes.sidebar')

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
          <section class="content-header">
    <h1>
      Business Dashboard
      <small>This is the landing page of the business dashboard</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="http://cc.xeedlabs.com"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="http://cc.xeedlabs.com/dashowner">Dashboard</a></li>
      <li class="active">Business</li>
    </ol>
  </section>

        <!-- Main content -->
        <section class="content">
          <!-- TIP -->
          
          <div class="alert alert-dismissable collapse" id="errorMessage">
            <button type="button" class="close" data-toggle="collapse" data-target="#errorMessage" aria-hidden="true">&times;</button>
            <h4><i class="icon fa" id="errorIcon"></i><span id="errorTitle"> Warning!</span></h4>
            <div></div>
          </div>

          <div id="content-dashboard">
            
  <div class="box box-primary collapse" id="businessAdd">
    <div class="box-header with-border">
      <h3 class="box-title">Add Business</h3>
    </div>
    <div id="businessAddForm_HBW"></div>
    <div class="overlay" id="businessAddLoading">
      <i class="fa fa-refresh fa-spin"></i>
    </div>
  </div>

  <div class="box box-primary collapse" id="businessEdit">
    <div class="box-header with-border">
      <h3 class="box-title">Edit Business</h3>
    </div>
    <div id="businessEditForm_HBW"></div>
    <div class="overlay" id="businessEditLoading">
      <i class="fa fa-refresh fa-spin"></i>
    </div>
  </div>

  <div class="box box-primary" id="businessCvsNotifications">
    <div class="box-header with-border">
      <button type="button" class="close" onclick="$('#businessCvsNotifications').hide()">×</button>
      <h3 class="box-title">Import CSV logs</h3>
    </div>
    <div class="box-body">
      <div class="table-log">
        <table class="table table-condensed table-hover">
        </table>
      </div>
    </div>
  </div>

  <!-- Default box -->
  <div class="box box-primary collapse in" id="businessTable">
    <div class="box-header with-border">
      <div>
        <a class="btn btn-app" onclick="cc.crud.business.add.create()">
          <i class="fa fa-plus"></i> Add Business
        </a>
  
          <a class="btn btn-app fileinput-button">
            <i class="fa fa-file-excel-o"></i> Upload CSV
            <input id="csv-upload" type="file" name="csv">
          </a>
          <a class="btn btn-app fileinput-button" href="http://cc.xeedlabs.com/downloads/businesses.csv">
            <i class="fa fa-file-excel-o"></i> CSV example
          </a>
          <a class="btn btn-app fileinput-button" href="http://cc.xeedlabs.com/downloads/businesses.xls">
            <i class="fa fa-file-excel-o"></i> XLS example
          </a>
        </div>

      <div id="csv-progress" class="progress" style="display:none">
          <div class="progress-bar progress-bar-success"></div>
      </div>
    </div>
    <div class="box-body">
      <div id="businessesTable_HBW"></div>
    </div><!-- /.box-body -->
    <div class="overlay" id="businessTableLoading">
      <i class="fa fa-refresh fa-spin"></i>
    </div>
  </div><!-- /.box -->
          </div>

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 2.3.0
        </div>
        <strong>Copyright &copy; 2014-2015 <a href="http://cc.xeedlabs.com">CertifiedComments.com</a>.</strong> Unlawful duplication prohibited by Law.
      </footer>

    

@endsection



