<?php

use App\Models\Lesson;
use Illuminate\Database\Seeder;

class ResourcesTableSeeder extends Seeder
{
    const SEEDED = 50;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < self::SEEDED; $i++) {
            $resource = factory(App\Models\Resource::class)->create();
            $lessons = Lesson::orderBy(\DB::raw('RAND()'))->take(rand(1, 3))->get();
            $resource->setLessons($lessons->pluck('id'));
        }
    }
}
