<?php

use Illuminate\Database\Seeder;

class RoadmapsTableSeeder extends Seeder
{
    const SEEDED = 10;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < self::SEEDED; $i++) {
            factory(App\Models\Roadmap::class)->create();
        }
    }
}
