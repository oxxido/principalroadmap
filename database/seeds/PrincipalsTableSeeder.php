<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\User;

class PrincipalsTableSeeder extends Seeder
{
    const SEEDED = 4;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = factory(User::class)->make();
        $user->email = env('TEST_PRINCIPAL_EMAIL', 'principal@user.com');
        $user->password = \Hash::make(env('TEST_PRINCIPAL_PASSWORD', 'secret'));
        $user->save();
        $user->admin()->create([]);
        $user->addToRoles(Role::PRINCIPAL);

        for ($i = 1; $i < self::SEEDED; $i++) {
            $model = factory(\App\Models\Principal::class)->create();
            $model->user->setRoles(Role::PRINCIPAL);
        }
    }
}
