<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        if (App::environment() !== 'production') {
            $this->call(RolesTableSeeder::class);
            $this->call(PermissionsTableSeeder::class);
            $this->call(AdminsTableSeeder::class);
            $this->call(AuthorsTableSeeder::class);
            $this->call(PrincipalsTableSeeder::class);
            $this->call(RoadmapsTableSeeder::class);
            $this->call(ModulesTableSeeder::class);
            $this->call(LessonsTableSeeder::class);
            $this->call(ResourcesTableSeeder::class);
        }

        Model::reguard();
    }
}
