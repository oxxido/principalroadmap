<?php

use App\Models\Principal;
use Illuminate\Database\Seeder;

class LessonsTableSeeder extends Seeder
{
    const SEEDED = 20;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < self::SEEDED; $i++) {
            $lesson = factory(App\Models\Lesson::class)->create();
            $principals = Principal::orderBy(\DB::raw('RAND()'))->take(rand(0, 3))->get();
            $lesson->setPrincipals($principals->pluck('id'));
        }
    }
}
