<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
                'name'        => 'admin',
                'label'       => 'Site administrator',
                'description' => 'Administrator of the whole site, can do anything on the system.',
                'created_at'  => Carbon::now(),
                'updated_at'  => Carbon::now()
            ],
            [
                'name'        => 'author',
                'label'       => 'Modules author',
                'description' => 'Creator of modules and lessons.',
                'created_at'  => Carbon::now(),
                'updated_at'  => Carbon::now()
            ],
            [
                'name'        => 'principal',
                'label'       => 'Principal',
                'description' => 'Some description for principal.',
                'created_at'  => Carbon::now(),
                'updated_at'  => Carbon::now()
            ]
        ]);
    }
}
