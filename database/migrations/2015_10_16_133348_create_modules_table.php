<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::create('modules', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('author_id')->unsigned();
                $table->foreign('author_id')->references('id')->on('authors')->onUpdate('cascade')->onDelete('cascade');
                $table->integer('roadmap_id')->unsigned();
                $table->foreign('roadmap_id')->references('id')->on('roadmaps')->onUpdate('cascade')->onDelete('cascade');
                $table->string('slug')->unique();
                $table->smallInteger('order')->unsigned()->defaults(1);
                $table->boolean('bonus')->defaults(false);
                $table->string('image_url')->nullable();
                $table->string('title');
                $table->string('description')->nullable();
                $table->timestamps();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::drop('modules');
        });
    }
}
