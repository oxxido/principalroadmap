<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonPrincipalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            Schema::create('lesson_principal', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('lesson_id')->unsigned();
                $table->foreign('lesson_id')->references('id')->on('lessons')->onUpdate('cascade')->onDelete('cascade');
                $table->integer('principal_id')->unsigned();
                $table->foreign('principal_id')->references('id')->on('principals')->onUpdate('cascade')->onDelete('cascade');
                $table->timestamps();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::transaction(function () {
            Schema::drop('lesson_principal');
        });
    }
}
