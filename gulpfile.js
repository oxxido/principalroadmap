var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

/*var styles  = [
        "normalize.css",
        "main.css"
    ]*/
var paths = {
    'css':         'public/css',
    'js':          'public/js',
    'jquery':      './resources/assets/bower/jquery/',
    'bootstrap':   './resources/assets/bower/bootstrap-sass/assets/',
    'fontawesome': './resources/assets/bower/font-awesome/',
    'adminLTE':    './resources/assets/bower/AdminLTE/dist/'
}

elixir(function(mix) {
    mix.sass("app.scss", 'public/css/', {
        includePaths: [paths.bootstrap + 'stylesheets/', paths.fontawesome + 'scss/', ]
    })
        
        .copy(paths.bootstrap + 'fonts/bootstrap/**', 'public/fonts')
        .copy(paths.fontawesome + 'fonts/**', 'public/fonts')
        .scripts([
            paths.jquery + "dist/jquery.js",
            paths.bootstrap + "javascripts/bootstrap.js"
        ], 'public/js/app.js', './')
        //copy adminlte files
        .copy(paths.adminLTE + 'css/AdminLTE.min.css', 'public/css/AdminLTE.min.css')
        .copy(paths.adminLTE + 'css/skins/skin-blue.min.css', 'public/css/AdminLTE.blue.min.css')
        .copy(paths.adminLTE + 'img/**', 'public/images/adminLTE')
        .copy(paths.adminLTE + 'js/app.min.js', 'public/js/AdminLTE.min.js')
        ;
});